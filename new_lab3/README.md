Written by David Wu (DamWu@ucsc.edu) and Rory Landau (rflandau@ucsc.edu).

# Overview
Httpserver is a simple server that responses to GET and PUT requests for files.
Its architecture is described in DESIGN.pdf.

# Compiling and Running
Httpserver can and should be compiled using the `make` command.
It can then be run via `./httpserver <ip> <port>`. Port is optional and defaults to 80.


# Capabilities and Limitations

# Style Guide

* Indentation is soft 4.

* Variable declaration, other than explicitly temporary variables, should be performed at the start of the variable's subroutine.

* Lines, ideally, should not exceed 80 characters and definitely no longer than 100 characters.
